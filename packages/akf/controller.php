<?php

namespace Concrete\Package\Akf;

use Concrete\Core\Package\Package;
use Concrete\Core\Page\Theme\Theme;
use Core;

defined('C5_EXECUTE') or die(_("Access Denied."));

class Controller extends Package {
  protected $pkgHandle  = 'akf';
  protected $pkgVersion = '1.0.0';

  protected $appVersionRequired = '5.7.0';

  public function getPackageDescription() {
    return t('Adds a the Akf theme.');
  }

  public function getPackageName(){
    return t('Akf');
  }

  public function install() {
    $pkg = parent::install();
    Theme::add('akf', $pkg);
  }

  public function on_start() {
    $environment = \Environment::get();
    $environment->overrideCoreByPackage('elements/express/form/form/attribute_key.php', $this);
    $environment->overrideCoreByPackage('elements/express/form/form/text.php', $this);
  }
}
