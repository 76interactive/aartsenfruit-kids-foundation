<?php
  Loader::packageElement('header', 'akf');
  $image = $c->getAttribute('page_image');
  if ($image) {
    $image_src = $image->getRelativePath();
    $image_title = $image->getTitle();
  } else {
    $image_src = $view->getThemePath() . '/assets/images/placeholder.png';
  }
?>

  <section class="section section--subpage">
    <div class="grid__container">
      <div class="grid__row">
        <div class="grid__col--12 grid__col--sm--12 page__title">
          <?php
            $c->getCollectionName();
            echo '<h1>' . $c->getCollectionName() . '</h1>'
          ?>
        </div>
        <div class="grid__col--5 grid__col--sm--12 page__section">
        <?php
          $a = new Area('Left');
          $a->display($c);
        ?>
        </div>
        
        <div class="grid__col--5 grid__col--sm--12 grid__shift--2 page__section">
        <?php
          $a = new Area('Right');
          $a->display($c);
        ?>
        <?php
          $a = new Area('Form');
          $a->display($c);
        ?>
        </div>
      </div>
    </div>
  </section>

<?php Loader::packageElement('footer', 'akf'); ?>
