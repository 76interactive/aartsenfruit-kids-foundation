<?php
  Loader::packageElement('header', 'akf');
  $image = $c->getAttribute('page_image');
  if ($image) {
    $image_src = $image->getRelativePath();
    $image_title = $image->getTitle();
  } else {
    $image_src = $view->getThemePath() . '/assets/images/placeholder.png';
  }
?>

  <section class="section">
    <div class="grid__container">
      <div class="grid__row">
        <div class="grid__col--8 grid__col--sm--12">
        <?php
          $a = new Area('Text');
          $a->display($c);
        ?>
        </div>
        
        <div class="grid__col--4 grid__col--sm--12 quote">
        <?php
          $a = new Area('Quote');
          $a->display($c);
        ?>
        </div>
      </div>
    </div>
  </section>

  <section class="section section__banner">
    <a href="http://aartsenfruit-kids-foundation.dev/stoergelukkig">
      <div class="section__banner--image">
        <?php
          if( is_object($c->getAttribute('banner_image')) ) {
        ?>
        <img src="<?php echo ($c->getAttribute('banner_image')->getVersion()->getRelativePath());?>">
        <?php
          }
        ?>
      </div>
      <div class="section__banner--text">
        <?php
          $c->getCollectionDescription();
          echo '<p>' . $c->getCollectionDescription() . '</p>'
        ?>
      </div>
    </a>
  </section>

  <section class="section">
    <div class="grid__container">
      <div class="grid__row">
        <div class="grid__col--12 grid__col--sm--12">
          <?php
            $a = new Area('Initiatieven');
            $a->display($c);
          ?>
        </div>
      </div>
    </div>
  </section>

<?php Loader::packageElement('footer', 'akf'); ?>
