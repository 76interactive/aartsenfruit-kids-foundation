//import '../scss/style.scss';
//import $ from 'jquery';

const isTablet = function isTablet() {
  return $(window).width() <= 768;
};

$(document).ready(function() {
  
  $('.navicon').click(function(e){
    e.preventDefault();
    $(this).toggleClass("open");
    $('.header__nav').stop().slideToggle();
  });

  /**
   * Slide nav for responsive navigation
   */

  //$('.header__item:has(ul)').append('<span class="header__item--sub"><i class="fa fa-chevron-down"></i></span>')

  const slideNav = function slideNav() {
    if (isTablet()) {
      $('.header__item--sub').show();
    } else {
      $('.header__item--sub').hide();
    }

    $('.header__navicon').stop().on('click', function(e) {
      e.preventDefault();
      $(this).toggleClass('header__navicon--open');
      if ($(this).hasClass('header__navicon--open')) {
        $('body').css({'overflow':'hidden'});
        $('.page, .header').animate({"left":"-250px"}, 250);
        $('.header__nav').animate({"right":"0"}, 250);
      } else {
        $('body').css({'overflow':'auto'});
        $('.page, .header').animate({"left":"0"}, 250);
        $('.header__nav').animate({"right":"-250px"}, 250);
      }
    });

    $('.header__item--sub').stop().on('click', function(e) {
      e.preventDefault();
      $(this).find('i').toggleClass('rotate');
      $('.header__sublist').stop().slideToggle();
    });
  };

  /*$(window).bind('resize', slideNav);
  slideNav();*/

  /**
   * Footer always sticks at the bottom of the page
   */

 

  /*$(window).bind('resize', stickyFooter);
  stickyFooter();*/

});

