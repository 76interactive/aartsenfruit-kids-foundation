    </div><!-- End Page Wrap -->

    <footer class="footer background__white color--black">
      <div class="grid__container">
        <div class="grid__row">
          <div class="grid__col--4 grid__col--sm--12">
            <?php
              $a = new GlobalArea('Footer-links');
              $a->display($c);
            ?>
          </div>
          <div class="grid__col--4 grid__col--sm--12">
            <?php
              $a = new GlobalArea('Footer-midden');
              $a->display($c);
            ?>
          </div>
          <div class="grid__col--4 grid__col--sm--12">
            <p>Copyright Aartsenfruit <?php echo date('Y'); ?></p>
          </div>
        </div>
      </div>
    </footer>

  </div><!-- End Page -->

  <?php Loader::element('footer_required') ?>
<!--
  <script src="<?php echo $view->getThemePath() ?>/assets/js/jquery-1.9.1.min.js"></script>
  <script src="<?php echo $view->getThemePath() ?>/assets/js/app.js"></script>
-->
</body>
</html>
