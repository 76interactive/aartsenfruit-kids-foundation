<?php
  $u = new User();
  $c = Page::getCurrentPage();
?>
<!doctype html>
<html lang="nl">
<head>
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0">
  <?php Loader::element('header_required') ?>
<!--  <link href="<?php echo $view->getThemePath() ?>/dist/css/app.css" type="text/css" rel="stylesheet" />-->
  <link href="<?php echo $view->getThemePath() ?>/assets/css/style.css" type="text/css" rel="stylesheet" />
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
  <script src="<?php echo $view->getThemePath() ?>/assets/js/app.js"></script>
</head>
<body<?php if ($u->isLoggedIn()):?> style="margin-top: 48px;"<?php endif; ?>>

  <div class="page"><!-- Start Page -->
    <div class="page__wrap"><!-- Start Page Wrap -->

      <section class="hero">
        <div class="hero-wrapper">
          <?php
            if( is_object($c->getAttribute('hero_image')) ) {
          ?>
          <div style="background-image: url('<?php echo ($c->getAttribute('hero_image')->getRelativePath());?>'); background-position: center; background-repeat: no-repeat; background-size: contain;"></div>
          <?php
            }
          ?>
        </div>
      </section>
      
      <!-- Header -->
      
      <header class="header header--fixed background__black--transparent">
        <div class="main-nav">
          <div class="grid__container">
            <a class="navicon x" href="#">
              <div class="navicon__bars"></div>
            </a>
            <div class="grid__row">
              <div class="grid__col--12">
                <nav class="header__nav float--right" <?php if ($c->isEditMode()): ?>style="width: 100%;"<?php endif; ?>>
                <?php
                  $a = new GlobalArea('Navigation');
                  $a->display($c);
                ?>
                </nav>
                <a class="header__navicon float--right" href="#">
                  <div class="header__navicon--bars"></div>
                </a>
              </div>
            </div>
          </div>
        </div>
        
<!--
        <div class="sub-nav">
          <div class="grid__container">
            <div class="grid__row">
              <div class="grid__col--12 grid__col--sm--12 page__section--subnav">
                <nav class="subheader__nav">
                  <?php
                    $a = new Area('subnav');
                    $a->display($c);
                  ?>
                </nav>
              </div>
            </div>
          </div>
        </div>
-->
      </header>

