<?php

/**
 * -----------------------------------------------------------------------------
 * Generated 2017-02-27T15:40:50+01:00
 *
 * DO NOT EDIT THIS FILE DIRECTLY
 *
 * @item      misc.do_page_reindex_check
 * @group     concrete
 * @namespace null
 * -----------------------------------------------------------------------------
 */
return array(
    'site' => 'Aartsenfruit Kids Foundation',
    'version_installed' => '5.7.5.9',
    'misc' => array(
        'access_entity_updated' => 1487753064,
        'latest_version' => '5.7.5.13',
        'do_page_reindex_check' => false,
    ),
    'seo' => array(
        'canonical_url' => '',
        'canonical_ssl_url' => '',
        'redirect_to_canonical_url' => 0,
        'url_rewriting' => 1,
    ),
    'cache' => array(
        'blocks' => true,
        'assets' => false,
        'theme_css' => true,
        'overrides' => false,
        'pages' => '0',
        'full_page_lifetime' => 'default',
        'full_page_lifetime_value' => null,
    ),
    'theme' => array(
        'compress_preprocessor_output' => true,
        'generate_less_sourcemap' => false,
    ),
    'debug' => array(
        'detail' => 'debug',
        'display_errors' => true,
    ),
);
