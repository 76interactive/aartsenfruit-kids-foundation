var gulp = require('gulp');
var minifyCss = require('gulp-minify-css');
var watch = require('gulp-watch');
var sass = require('gulp-ruby-sass');
var rename = require('gulp-rename');

var assetsPath = 'packages/magenta/themes/magenta/assets';

// Gulp assets
// ------------------------------

gulp.task('watch', function() {
  gulp.watch(assetsPath + '/scss/**/*.scss', ['sass']);
});

gulp.task('sass', function () {
  return sass(assetsPath + '/scss/style.scss', { sourcemap: true })
    .pipe(gulp.dest(assetsPath + '/css'))
});

gulp.task('minify-css', ['sass'], function() {
  return gulp.src(assetsPath + '/css/style.css')
    .pipe(minifyCss({compatibility: 'ie8'}))
    .pipe(rename({suffix: '.min'}))
    .pipe(gulp.dest(assetsPath + '/css'));
});

// Gulp init
// ------------------------------

gulp.task('default', ['sass', 'watch'], function() {

});